function printInput(){
	let nickname = prompt("Enter your nickname");
	console.log("Hi, " + nickname);
}
// printInput();

// name is a parameter
function printName(name){
	console.log("My name is " + name);
}
// ("Harsha") is an argument
// printName("Harsha");

// Variables passed as an argument
let sampleVariable = "James";
printName(sampleVariable);

function printAge(age){
	console.log("I am " + age + " years old");
}
printAge(19);

function printLocation(location){
	console.log("I live in " + location);
}
printLocation("Mumbai");

function checkDivisibiltyBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + "divisible by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibiltyBy8(751);

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);
// Displaying more information about the argumentFunction
console.log(argumentFunction);

// Use Multiple Parameters
function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}
createFullName('John', typeof false, 782);
createFullName("Peter", "Parker");
createFullName("Avengers", 'Justice', "League", 982);

function printFullName(middleName, firstName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}
printFullName('John', 'Peter', 'Smith');
printFullName('Peter', 'Parker');
printFullName("Avengers", 'Justice', "League", 982);

// Return statement
// Allows to output value from a function to be passed to the line/ block of code that invoced the function.

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This message will not be printed");
}
let completeName = returnFullName("Peter", "B.", "Parker");
console.log(completeName);

function returnAdress(city, country){
	let fullAddress = city + ' , ' + country;
	return fullAddress;
}
let myAddress = returnAdress("Mumbai", "India");
console.log(myAddress);

function printPlayerInfo(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);